'use strict';

var fs = require('fs');

var file = fs.readFileSync('someJSONfile.json');
var tmp = JSON.parse(file);

for (var someProperty in tmp)
{
	console.log("Property Type is a : " + typeof tmp[someProperty] + ". Property name is : " + someProperty + ". The value is : " + tmp[someProperty]);
}
