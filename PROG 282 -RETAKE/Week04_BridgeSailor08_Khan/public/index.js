

function parse() {
    'use strict';
	//var jsonData = [];
	var jsonData;
	
    $.getJSON('/readJson', function(data) {

        //jsonData = JSON.stringify(data);
        jsonData = data;
    })
        .success(function() {
            console.log("csc: success. Loaded index.json");
            //clear nameDiv
            $("#nameDiv").html("");
            $("#nameDiv").append(jsonData);
            
            //loop and display each item in json file
            $.each(jsonData, function(i, item) {
            	$("#nameDiv").append(item.firstName + ' ' + item.lastName + '<br/>');
        	});
        })
        .error(function(jqXHR, textStatus, errorThrown) {
            alert("error calling JSON. Try JSONLint or JSLint: " + textStatus + errorThrown);
        })
        .complete(function() {
            console.log("csc: completed call to get index.json");
        });
}

function mdRead() {
    'use strict';
	var rtData;
	
    $.get('/readMD', function(data) {
        rtData = data;
    })
        .success(function() {
            console.log("csc: success. Loaded index.json");
            //clear nameDiv
            //$("#nameDiv").html("");
            $("#namePre").html("");
            $("#namePre").html("<strong>Charlie!</strong> cant display MD data if Read Parse Jason is clicked first and then Read MD file is clicked. Works if Read MD file is clicked first. Looking the debugger the data is read successfully. <br/><br/>");
            $("#namePre").append(rtData);
        })
        .error(function(jqXHR, textStatus, errorThrown) {
            alert("error calling JSON. Try JSONLint or JSLint: " + textStatus + errorThrown);
        })
        .complete(function() {
            console.log("csc: completed call to get index.json");
        });
}



$(document).ready(function() {
    'use strict';
    $("#btnJson").click(parse);
    $("#btnMD").click(mdRead);
});
