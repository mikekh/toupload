/**
 * @author Mike Khan  
 * See http://www.addyosmani.com/resources/essentialjsdesignpatterns/book/
 */

if ( typeof define !== 'function') {
    var define = require('amdefine')(module);
}

define(function(require) {

    function Sloop(options) {'use strict';
		// A constructor for defining new Sloop
        // some defaults
        this.sailCount = options.sailCount || 2;
        this.state = options.state || "brand new";
        this.color = options.color || "silver";
        //this.mizzen = options.mizzen || false;
        this.keel = options.keel || true;
        //this.topsail = options.topsail || false;
    }

    return Sloop;

}); 