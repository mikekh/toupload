define(function(require) {'use strict';

    var elf = {};
    elf.run = {};

    elf.ClickEvents = ( function() {
            var listItem = $(".listItem");
            var intro = $("#intro");

            function ClickEvents() {
                $(intro).html("ClickEvents is loaded. Click items on this page.");
                $(intro).addClass('blue');
                $(listItem).click(listClick);                
            }

            var listClick = function(event) {
                var clickText = event.target.innerText;
                var prompt = "You clicked: ";
                $(intro).html(prompt + clickText);
                jsonRead(clickText);
            };
            
            function jsonRead(clickText) {
            	'use strict';
            var route = '/' + clickText;
            var jsonData;

            $.getJSON(route, function(data) {

                //jsonData = JSON.stringify(data);
                jsonData = data;
            })
                .success(function() {
                    console.log("csc: success. Loaded index.json");
                    displayJsonData(jsonData);
                })
                .error(function(jqXHR, textStatus, errorThrown) {
                    alert("error calling JSON. Try JSONLint or JSLint: " + textStatus + errorThrown);
                })
                .complete(function() {
                    console.log("csc: completed call to get index.json");
                });
            };
            
            function displayJsonData(jsonData) {
    			'use strict';
    			
    		/*
            //clear nameDiv
            //$("#nameDiv").html("");
            //$("#nameDiv").append(jsonData);
            
            //loop and display each item in json file
            $.each(jsonData, function(i, item) {
            	$("#nameDiv").append(item.firstName + ' ' + item.lastName + '<br/>');
          
        		});
        		*/
        		$("#result").html(jsonData.result);
        		$("#route").html(jsonData.route);
        		$("#message").html(jsonData.message);
        		
			};

            return ClickEvents;

        }());

    return elf.ClickEvents;

});
