var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.send('index', { "result": 'Hello from Address' });
});

router.get('/read', function(request, response) {
    response.send({ "result": "Reading from Week07InClassRoute_Khan routes/address.js" });
});


module.exports = router;
