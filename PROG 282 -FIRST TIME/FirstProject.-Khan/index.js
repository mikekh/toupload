/**
 * @author Mike Khan
 */

var MyObject = (function() {
	'use strict';
	
	//constructor
	function MyObject() {
		$("#content").css( { "color": "green" } );
		$('#content').html('Contnet and color changed on document ready');
	}
	
	return MyObject;
})();

$(document).ready(function() {
	// in this case no need to assign to a var
	new MyObject();
});

// following will work as well.
//Uncomment code below and comment out the code above
/*
$(document).ready(function() {
	// in this case no need to assign to a var
	$("#content").css( { "color": "green" } );
	$('#content').html('Contnet and color changed on document ready');
})
*/