/*
 * @author: Charlie Calvert
 */

var TestCanvas = (function() {
	//unit test flag
	ELF.run.CanvasTest = null;


	var myCanvas;
	var contextOrcDead;

	// Constructor
	function TestCanvas() {		
		//myCanvas = document.getElementById('orcCanvas');
		//context = myCanvas.getContext('2d');
		
		contextOrcDead = getCanvas();
		diplayOrcDead();
		
		//set test flag		
		ELF.run.CanvasTest = "Success";
	}
	
	function diplayOrcDead(){
		contextOrcDead.lineWidth = 15;
		contextOrcDead.strokeStyle = "black";
		contextOrcDead.fillStyle = "red";
		contextOrcDead.fillRect(0, 0, 200, 100);
		contextOrcDead.strokeRect(0, 0,  200, 100);
		contextOrcDead.fillStyle = "black";
		contextOrcDead.font = "20px Comic Sans MS";
		contextOrcDead.fillText("ORC KILLED!!", 40, 50)
	}
	
	
	var getCanvas = function() {
		var canvas = document.getElementById('orcCanvas');
		if (canvas !== null) {
			var context = canvas.getContext('2d'); 
			return context;
		} else {
			$("#debugs").css({
				backgroundColor : "blue",
				color: "red"
			});
			$("#debugs").html("Could not retrieve canvas");
			return null;
		}
	};

	return TestCanvas;
})();


new TestCanvas();

$(document).ready(function() {'use strict';
	new TestCanvas();
});
