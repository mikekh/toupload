/**
 * @author MIKE
 */
	
	
var GridGame = (function() {
	//Flag for unit test
	ELF.run.GridGameTest = null;
	
	function GridGame(){
		showConflict();
		showGrid();
		ELF.run.GridGameTest = "Success";
	}

	function showGrid() {
	
		ELF.run.GridGameTest = "Success";
		
		$.ajax({
			type: 'GET',
			url: '/TestGrid',
			dataType: 'html',
			success: function(data) {
				// var myHtml = $(data + ' #myCanvasInsert')
				$('#gridDiv').html(data);
				
				//hide everything except for the grid			
				$("#keysListener01").hide();
				$('#canvasGridHeading').hide();
				$('#testConflictAnchor').hide();
			},
			error: showError      
		    }); 
		    
		// disable main character movement. DOES NOT WORK	
		//ELF.run.keyBoard.toggleKeys(false);	
		//$("#showProp").html("keyListenerState Property is: " +  ELF.run.keyBoard.listenerState());
	}

	function showConflict() {
		$.ajax({
			type: 'GET',
			url: '/TestScoring',
			dataType: 'html',
			success: function(data) {

				$('#talbesDiv').html(data);
				
				//hide every thing except for the tables
				$('#testGridAnchor').hide();
				$("#testConflict").hide();
				$("#btnRead").hide();
				$("#btnWrite").hide();
				$("#btnStrike").hide();
			},
			error: showError      
		    }); 
		    		 
	}
	
	function insertGrid() {
		$('#gridDiv').load("TestGrid.html #mainCanvas");
	}
	
	var showError = function(request, ajaxOptions, thrownError) {
	showDebug("Error occurred: = " + ajaxOptions + " " + thrownError );
	showDebug(request.status);
	showDebug(request.statusText);
	showDebug(request.getAllResponseHeaders());
	showDebug(request.responseText);
	};
	
	var showDebug = function(textToDisplay) {
        $("#debug").append('<li>' + textToDisplay + '</li>');
    };
	
	return GridGame;
})();

$(document).ready(function() {'use strict';
	new GridGame();
});
