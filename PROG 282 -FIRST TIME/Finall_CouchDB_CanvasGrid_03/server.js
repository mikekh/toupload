/**
 * @author mike khan
 * 
 * Refernces:
 * 	http://dailyjs.com/2012/01/26/effective-node-modules/
 */
 
/*jshint browser: true, devel: true, strict: true */
/*global require: true, process: true */

var openid = require('openid');
var express = require('express');
var app = express();
var fs = require('fs');
var querystring = require('querystring');

//jade and stylus
var routes = require('./routes')
var user = require('./routes/user')
var http = require('http')
var path = require('path');
     // all environments
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(require('stylus').middleware(__dirname + '/public'));
app.use(express.static(path.join(__dirname, 'public')));



//couchDB
//var dbName = 'khan_test';
var dbName = 'prog282_khan';
var nano = require('nano')('http://127.0.0.1:5984');


//OoenID
var url = require('url');
var querystring = require('querystring');
var relyingParty = new openid.RelyingParty(
	'http://localhost:30025/go', // Verification URL (yours)
	null, // Realm (optional, specifies realm for OpenID authentication)
	false, // Use stateless verification
	false, // Strict mode
	[]
);

//load Templator
var templater = require('./Library/Templater');
console.log(templater);

//setup listening port
var port = process.env.PORT || 30025;

app.get('/', function(req, res) {
	
	//res.redirect('/all');
	//res.redirect('/TestGrid');
	//res.redirect('/TestScoring');
	//res.redirect('/TestCanvas');
	//res.redirect('/UnitTest01');

	var html = fs.readFileSync('Public/index.html');
	res.writeHeader(200, {"Content-Type": "text/html"});
	res.end(html);
	
/*	
	var html = fs.readFileSync('Public/openID_logon.html');
	res.writeHeader(200, {"Content-Type": "text/html"});
	res.end(html);
*/
});



app.get('/index', function(req, res) {
	var html = fs.readFileSync('Public//index.html');
	res.writeHeader(200, {"Content-Type": "text/html"});
	res.end(html);
});



app.get("/attachHtml", function(request, response) {'use strict';
   console.log('In attachHtml' );
   console.log('In attachHtml query is : ' + request.query);
   console.log('In attachHtml record Name is:' + request.query.doc);
   console.log('In attachHtml file name is: ' + request.query.key);
   

    //Null means we are trying to do an insert
   doHTMLinsert(null, 
       response, 
       request.query.key, 
       request.query.doc);
});


var doHTMLinsert = function(rev, response, key, docName) {
   console.log('In doHTMLinsert' );
   console.log('In doHTMLinsert query rev : ' + rev);
   console.log('In doHTMLinsert record Name is:' + docName);
   console.log('In doHTMLinsert file name is: ' + key);

    fs.readFile(__dirname + '/Templates/' + docName, function(err, data) {
        if (!err) {
            var prog = nano.db.use(dbName);
            prog.attachment.insert(key, docName, data, 'text/html', rev,
                function(err1, body) {
                if (!err1) {
                    console.log(body);
                    response.send({"Result":"Success"});
                } else {
                    console.log(" error doing HTML insert. The record may already exist, try doing update. ");
                    reportErrorPrivate(err1);
                    response.send(500, " error doing HTML insert. The record may already exist, try doing update. ");
                }
            });
        } else {
            console.log("error fs.read in doHTMLinsert failed");
            response.send(500, "fs.read in doHTMLinsert failed");
        }
    }); 
};

app.get("/getAttachedHtml", function(request, response) {'use strict';
   console.log('/getAttachedHtml called');
   
   var prog = nano.db.use(dbName);

    prog.attachment.get('insertConflictHTML', 'insertFightOrc.html', function(err, body) {
        if (!err) {
            console.log(body);
            response.send(body);
        } else {
            reportErrorPrivate(err);
            response.send(500, err);
        }   
    }); 
});


app.get('/createDatabase', function(request, response) {'use strict';
	 console.log('create called.');
    nano.db.create(dbName, function(err, body) {
        if (!err) {
            console.log(body);
            response.send({"Result":"Success"});
        } else {
        	console.log('Could not create database');
        	//reportErrorPrivate(err);
        	response.send(500, err);
        	return;

        }        
	});
});

app.get('/readJson', function(request, response) {
    console.log('In readJson called: ' + JSON.stringify(request.query));
    var prog = nano.db.use(dbName);
    
    prog.get(request.query.docName, function(error, existing) {
        if(!error) { 
            console.log(existing);
            response.send(existing);
        }  else {
        	reportErrorPrivate(error) ;   
	        response.send(500, error);
        }
    });
    console.log('Exiting Get readJson');
});



app.get('/writeRecordDB', function(request, response) {'use strict';
 	console.log('writeRecordDB called: ' + JSON.stringify(request.query));
 	

    console.log('in writeRecordDB record name: ' + JSON.stringify(request.query.recordName));
    console.log('in writeRecordDB  record value: ' + JSON.stringify(request.query.recordValue));
    
    var recordType = JSON.parse(request.query.recordType);
    console.log('in writeRecordDB record type is: ' + recordType);
    

    // using the switch statement because for some reason have pass literal for the first pram???
    switch (recordType){
    	case 0:
    	  sendToCouch(response,  { 'TableField' : request.query.recordValue }, request.query.recordName);
    	  break;    	
    	case 1:
    	  sendToCouch(response,  { 'TableField' : request.query.recordValue }, request.query.recordName);
    	  break;
    	case 2:
    	  sendToCouch(response,  { 'GridArrayField' : request.query.recordValue }, request.query.recordName);
    	  break;    	  
    	case 3:
    		// see it this takes care of the problem or numbers as stings
    		//var arryData = JSON.parse(request.query.recordValue); 
    		//sendToCouch(response,  { 'OrcArrayField' : arryData }, request.query.recordName);
    		
    	  sendToCouch(response,  { 'OrcArrayField' : request.query.recordValue }, request.query.recordName);
    	  break;
    	default:
    		console.log('writeRecordDB: did not recognize data');
    }
    //sendToCouch(response,  { 'TableFiled' : request.query.recordValue }, request.query.recordName);
    
    console.log('Exiting writeRecordDB');
});


var sendToCouch = function(response, data, docName) { 'use strict';
	  console.log('in sendToCouch: ' + docName);
	  console.log('in sendToCouch: ' + data);

//return;

    var prog = nano.db.use(dbName);
    prog.get(docName, function(error, existing) {
        if(!error) { 
            console.log("Document exists. Doing Update.");
            console.log(existing);
            console.log(existing._rev);
            data._rev = existing._rev;
            console.log("Before update the data is: " + JSON.stringify(data));
//return;
            doInsert(response, data, docName);
        }  else {
            console.log("Document does not exist. Doing insert.");
            //console.log(error);
            reportErrorPrivate(error) ; 
            console.log('in sendToCouch: ' + docName);
	  		console.log('in sendToCouch: ' + data); 
            doInsert(response, data, docName);
        }
    }); 
   
   console.log('Existing sendToCouch');
};


var doInsert = function(response, data, docName) {'use strict';
	//console.log('in doInsert');
	console.log('In doInsert docName is: ' + docName);
    console.log('doInsert data is: ' + data);


    var prog = nano.db.use(dbName);
    prog.insert(data, docName, function(err, body) {
    console.log('In doInsert callback');
        if (!err) {
            console.log(body);
            response.send({
                "Result" : "Success"
            });
            return;
        } else {
            //console.log(err);
            reportErrorPrivate(err) ;  
            //response.send(500, err);
            return;
        }
    });
};

// this function is called from OpenID if the provider url found to be  Authenticate
app.get('/go', function(req, res) {
	relyingParty.verifyAssertion(req, function(error, result) {
		//res.writeHead(200);
		//res.end(!error && result.authenticated ? 'Success :)' : 'Failure :(');
		
		if(!error && result.authenticated)
		{
			res.redirect('/index');
		} 
	});
});



app.get('/authenticate', function(req, res) {
	console.log("Authenticate called");
	console.log(req.query);
	// User supplied identifier
	var query = querystring.parse(req.query);
	console.log("Parsed Query: " + query);
	var identifier = req.query.openid_identifier;
	console.log("identifier: " + identifier);

	// Resolve identifier, associate, and build authentication URL
	relyingParty.authenticate(identifier, false, function(error, authUrl) {
		if (error) {
			res.writeHead(200);
			res.end('Authentication failed: ' + error.message);
		} else if (!authUrl) {
			res.writeHead(200);
			res.end('Authentication failed');
		} else {
			res.writeHead(302, {
				Location : authUrl
			});
			res.end();
		}
	});
});

app.get('/TestCanvas', function(req, res) {
	console.log("TestCanvas called");
	var html = fs.readFileSync('Public/TestCanvas.html');
	res.writeHeader(200, {"Content-Type": "text/html"});
	res.end(html);
});

app.get('/UnitTest01', function(req, res) {
	console.log("UnitTest01 called");
	var html = fs.readFileSync('Public/UnitTest01.html');
	res.writeHeader(200, {"Content-Type": "text/html"});
	res.end(html);
});

app.get('/TestGrid', function(req, res) {
	console.log("TestGrid called");
	var html = fs.readFileSync('Public/TestGrid.html');
	res.writeHeader(200, {"Content-Type": "text/html"});
	res.end(html);
});

 app.get('/TestScoring', function(request, response) { 'use strict';
 	console.log("TestScoring called");

	var htmlTemplate = templater.template;
	var html = htmlTemplate.addTable1(
		__dirname + '/Public/TestScoring.html',
		__dirname + '/Templates/HeroTable.html',
		__dirname + '/Templates/OrcTable.html'
	);
	response.writeHead(200, {"Content-Type": "text/html"});
	response.end(html);
});

app.get('/readHero', function(request, response){
	console.log('read Hero called.');
	var obj;
	
	function readData(err, data) {
		if (err) throw err;
		obj = JSON.parse(data);
		response.send(obj);
	}

	// Asynch call 
	fs.readFile(__dirname + '/Data/Hero.json', 'utf8', readData);
});


app.get('/readOrc', function(request, response){
	console.log('readOrc called.');
	var obj;
	
	function readData(err, data) {
		if (err) throw err;
		obj = JSON.parse(data);
		response.send(obj);
	}

	// Asynch call 
	fs.readFile(__dirname + '/Data/Orc.json', 'utf8', readData);
});

app.get('/readArray', function(request, response){
	console.log('Read called: ' + JSON.stringify(request.query));
	var obj;
	
	function readData(err, data) {
		if (err) throw err;
		obj = JSON.parse(data);
		response.send(obj);
	}

	// Asynch call 
	fs.readFile(request.query.fileName, 'utf8', readData);
});

app.get('/writeHero', function(request, response) {
	console.log('writeHero called: ' );
	var person = request.query;
	var personString = JSON.stringify(person, null, 4);
	console.log('PersonString: ' + personString);
	

	fs.writeFile(__dirname + '/SavedGames/Hero.json', personString, 'utf8', function(err, data) {
		if (err) throw err;
		console.log('Hero saved!');
	});
	response.send('{"result":"success"}');
});

app.get('/writeOrc', function(request, response) {
	console.log('writeOrc called: ');
	var person = request.query;
	var personString = JSON.stringify(person, null, 4);
	console.log('PersonString: ' + personString);
	
	fs.writeFile(__dirname + '/SavedGames/Orc.json', personString, 'utf8', function(err, data){
		if (err) throw err;
		console.log('Orc saved!');
	});
	response.send('{"result":"success"}');
});

app.get('/all', function(request, response) { 'use strict';
 	console.log("Game ALL  called");

	var htmlTemplate = templater.template;
	var html = htmlTemplate.addTable1(
		__dirname + '/Public/GridGame.html',
		__dirname + '/Templates/HeroTable.html',
		__dirname + '/Templates/OrcTable.html'
	);
	response.writeHead(200, {"Content-Type": "text/html"});
	response.end(html);
});


app.get('/view/index', routes.index);
app.get('/view/page02', routes.page02);
app.get('/view/page03', routes.page03);
app.get('/view/page04', routes.page04);


app.use('/', express.static(__dirname + '/Public'));
app.use('/', express.static(__dirname + '/Templates'));


var reportErrorPrivate = function(error) {
        console.log('==========================')
        console.log('Error: ' + error.error);
        console.log('Status Code: ' + error['status_code']);
        console.log('Reason: ' + error.reason);
        console.log('Description: ' + error.description); 
};

app.listen(port);
console.log('Listening on port :' + port);

