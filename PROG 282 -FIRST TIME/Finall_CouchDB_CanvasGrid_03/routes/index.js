
/*
 * GET home page.
 */

exports.index = function(req, res){
  res.render('index', { title: 'Prog282 Week09 ', test01: 'This my test' });
};

exports.page02 = function(req, res){
  res.render('page02', { title: 'Prog282 Week09 ', test02: 'This is variable test02'});
};

exports.page03 = function(req, res){
  res.render('page03', { title: 'Prog282 Week09 ', test03: 'This is variable test03'});
};

exports.page04 = function(req, res){
  res.render('page04', { title: 'Prog282 Week09 ', test02: 'This is variable test02', test01: 'This is variable test01'});
};
