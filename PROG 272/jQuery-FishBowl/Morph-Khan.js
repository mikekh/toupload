/**
 * @author Mike Khan
 */

var MyObject = (function() {
    'use strict';
	
	var images = ["Fish.jpg", "FishInBowl.jpg"];
	var imgs = [];
	
	// Constructor 
    function MyObject() {
    	// Initializes images        
        for (var i = 0; i < images.length; i++) {
        	imgs[i] = new Image();
        	imgs[i].src = images[i];   
        };
        // Call private methods without using this.
        //run();                	     	
    };	
	
	// Private method for loading images
	var setImage = function (imageIndex) {				
	   	$("#myImage").attr({
	   		src: imgs[imageIndex].src        	
	    }); 
	}; 
	
	
	// Private method to initialize application
    //var run = function () {
       	//$("#changeMe").html("This is my new text.")
       //setImage(0);
    //};  
	
		
	
    MyObject.prototype.run = function () {
       	$("#changeMe").html("No Button has been clicked yet.")
		setImage(0);
    };      

    MyObject.prototype.runButton1 = function () {
		$("#pageBody").css({ "backgroundColor": "Beige" } );
    	$("#myHeading").html("Button 1 Clicked. Heading Changed");
		$("#myHeading").css({ "color": "red" });
       	$("#changeMe").html("You have clicked button One.");
		$("#changeMe").css( { "color": "red" } );
       	$(".paraColor").css( { "color": "blue" } );
		$("#myList").empty();
		$("#myList").css( { "color": "green" } );
       	$("#myList").append("<li>Clicked 1. Item One.</li>")
		
		setImage(1);
    };   
	
	MyObject.prototype.runButton2 = function () {
		$("#pageBody").css({ "backgroundColor": "lightblue" } );
    	$("#myHeading").html("Button 2 Clicked. Heading Changed");
		$("#myHeading").css({ "color": "green" });
       	$("#changeMe").html("You have clicked button Two.")
		$("#changeMe").css( { "color": "green" } );
       	$(".paraColor").css( { "color": "red" } );
		$("#myList").empty();
		$("#myList").css( { "color": "black" } );
       	$("#myList").append("<li>Clicked 2. Item One.</li>")
		$("#myList").append("<li>Clicked 2. Item Two.</li>")
		
		setImage(0);
    };   
	
	
	MyObject.prototype.runButton3 = function () {
		$("#pageBody").css({ "backgroundColor": "plum" } );
    	$("#myHeading").html("Button 3 Clicked. Heading Changed");
		$("#myHeading").css({ "color": "navy" });
       	$("#changeMe").html("You have clicked button Three.")
		$("#changeMe").css( { "color": "navy" });
       	$(".paraColor").css( { "color": "yellow" } );
		$("#myList").empty();
		$("#myList").css( { "color": "red" } );
       	$("#myList").append("<li>Clicked 3. Item One.</li>")
		$("#myList").append("<li>Clicked 3. Item Two.</li>")
		$("#myList").append("<li>Clicked 3. Item Three.</li>")
		
		setImage(1);
    };  
	
    return MyObject;
})();

// This will be called when page is ready
$(document).ready(function() {
	myObject = new MyObject();
	myObject.run();
});

