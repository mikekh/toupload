
function MapDisplay()
{	
	// We have to give it a unique name in this context
	thisMapDisplay = this;	 
}

MapDisplay.prototype.clearResponse = function()
{
	$('#response').empty();
};

MapDisplay.prototype.isValidRow = function(row) {
	return !( (row.MiddleName === undefined) || 
			(row.MiddleName === '[object Object]') || 
			(row.MiddleName === '-') );	
};


MapDisplay.prototype.displayRow = function(row) {
	textToDisplay = row.cityName + " - latitude: " + row.latitude  + " longitude: " + row.longitude;
	var coreString = '<li><input id=' + row.itemName + 
			  ' cityName="' + row.cityName +
			  '" latitude=' + row.latitude + 
			  ' longitude=' + row.longitude + 
			  ' type=radio name=responseGroup />';
	$('#response').append(coreString + 
			textToDisplay + '</li>');	
};

MapDisplay.prototype.showResponse = function(textToDisplay)
{
	$('#response').append('<li>' + textToDisplay + '</li>');
};

MapDisplay.prototype.showDebug = function(textToDisplay)
{
	$("#debug").append('<li>' + textToDisplay + '</li>');
};

MapDisplay.prototype.showError = function(request, ajaxOptions, thrownError) {
	thisDisplay.showDebug("Error occurred: = " + ajaxOptions + " " + thrownError );
	thisDisplay.showDebug(request.status);
	thisDisplay.showDebug(request.statusText);
	thisDisplay.showDebug(request.getAllResponseHeaders());
	thisDisplay.showDebug(request.responseText);
};