ELF.own.MapCore = (function(displayInit, initUtilities,initGeoCode) {

	var display = null;
	var selectedItem = '';
	var utilities = null;
	var citiesList = null;
	var geoCode = null;

	function MapCore(displayInit, initUtilities,initGeoCode) {
		display = displayInit;
		utilities = initUtilities;
		geoCode = initGeoCode;

		// click GetCities first. other buttons disables
		$('#saveCities').attr("disabled", true);
		$('#insertCity').attr("disabled", true);
		$('#deleteitem').attr("disabled", true);
		$('#getLocaton').attr("disabled", true);
	}

	var radioSelection = function() {
		selectedItem = $("input[name=responseGroup]:checked").attr('id');
		CityName = $("input[name=responseGroup]:checked").attr('cityName');
		Latitude = $("input[name=responseGroup]:checked").attr('latitude');
		Longitude = $("input[name=responseGroup]:checked").attr('longitude');
		$('#cityName').val(CityName);
		$('#latitude').val(Latitude);
		$('#longitude').val(Longitude);
		
		$('#description').html("City: " + CityName + " Latitude:  " + Latitude + " Longitude: " + Longitude);
	};

	var clearResponse = function(debugMessage) {
		display.clearResponse();
		//display.showDebug(debugMessage);
	};

	MapCore.prototype.getCities = function(callback) {
		clearResponse("Get Cities called");
		request = $.ajax({
			type : "get",
			url : '/getCities',
			cache : false,
			dataType : "json",
			success : function(data) {
				citiesList = data;
				geoCode.listMarkers(citiesList);
				showCities();
				$('#responseGroup').change(radioSelection);
				$("input[name=responseGroup]:radio:first").attr('checked', true);
				radioSelection();
			},
			error : display.showError
		});
      
		// enable buttons disables
		$('#saveCities').attr("disabled", false);
		$('#insertCity').attr("disabled", false);
		$('#deleteitem').attr("disabled", false);
		$('#getLocaton').attr("disabled", false);
	};
	
	MapCore.prototype.saveCities = function() {
		clearResponse("Save Cities called");
		var data = { details: 'cities', data: JSON.stringify(citiesList) };
		$.ajax(
		{
			type: "POST",
			url: '/saveCities',
			dataType: "json",
			cache: 'False',
			data: data, 
			success: function(data) {
				//display.showDebug(data.result);
			},
			error: display.showError			
		});	
		
		//no list displayed so refresh the map and set it back to seatle
		GeoCode(47.6062095, -122.3320708);
		
		// Clear direction display panel 
      $('directions').html('');
	};

	MapCore.prototype.insertPresident = function() {
		names = getNames();
		if (names) {
			insertRecord(names.citytName, names.latitude, names.longitude);
		}
	};

	function getNames() {
		var names = {};
		names.citytName = $.trim($('#cityName').val());
		names.latitude = $.trim($('#latitude').val());
		names.longitude = $.trim($('#longitude').val());
		if (!utilities.readyForUpdate(names.citytName, names.latitude, names.longitude)) {
			alert("Please enter all the city information");
			return null;
		}
		return names;
	};

	var insertRecord = function(city, latitude, longitude) {
		clearResponse('insertRecord called ');
		
		var pName = city + " " + latitude + " " + longitude;
		//display.showDebug("inserting: " + pName);
		var city = new ELF.own.MapJson(city, latitude, longitude);
		var query = city.toJSON();
		citiesList.push(query);
		showCities();
	};

	MapCore.prototype.deleteItem = function() {
		clearResponse('Called delete item: ' + selectedItem);
		query = "itemName=" + selectedItem;
		utilities.deleteFromArray(citiesList, selectedItem);			
		showCities();	
	};


	var showCities = function() {
		display.clearResponse();
		var count = 0;
		$(citiesList).each(function() {
			$(this).each(function() {
				this.itemName = 'item' + count++;
				display.displayRow(this);
			});
		});
	};
	
	MapCore.prototype.cityCoordinates = function() {
		geoCode.getCoordinates();	
	};
	
	MapCore.prototype.getDirections = function() {
		geoCode.getDirections();	
	};
		
	return MapCore;

})();

$(document).ready(function() {
	var cities = new ELF.own.MapCore(new MapDisplay(), new ELF.own.MapUtils(), new GeoCode(47.6062095, -122.3320708));
	$('button:#getCities').click(cities.getCities);
	$('button:#insertCity').click(cities.insertPresident);
	$('button:#saveCities').click(cities.saveCities);
	$('button:#deleteitem').click(cities.deleteItem);
	$('button:#getLocaton').click(cities.cityCoordinates);
	$('button:#getDirections').click(cities.getDirections);
});


