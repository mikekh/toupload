/*global ELF:true*/

ELF.own.MapJson = (function() {

	function withValue(value) {
		var d = withValue.d || (withValue.d = {
			enumerable : false,
			writable : false,
			configurable : true,
			value : null
		});
		d.value = value;
		return d;
	}

	function MapJson(initName, initLatitude, initlongitude) {
		Object.defineProperty(this, "cityName", withValue(initName));
		Object.defineProperty(this, "latitude", withValue(initLatitude));
		Object.defineProperty(this, "longitude", withValue(initlongitude));
	}

	MapJson.prototype.initFromJSON = function(json) {
		this.cityName = json.cityName;
		this.latitude = json.latitude;
		this.longitude = json.longitude;
	};

	MapJson.prototype.toJSON = function() {
		return {
			cityName : this.cityName,
			latitude : this.latitude,
			longitude : this.longitude
		};
	};

	return MapJson;
})();
