ELF.own.MapUtils = (function() {

	function MapUtils() {
		
	}
	
	var isEmptyString = function(value) {
		return ((!value) || (value === "") );
	}

	MapUtils.prototype.readyForUpdate = function(cityName, latitude, longitude) {
		var failure = isEmptyString(cityName) || isEmptyString(latitude) || isEmptyString(longitude);
		return !failure;
	}

	MapUtils.prototype.deleteFromArray = function(array, value) {
		var len = array.length - 1;
		while (len > 0) {
			pres = array[len--];
			if (pres.itemName === value) {
				array.splice(len + 1, 1);
				return;		
			}
		}
	}
	
	return MapUtils;
})(); 