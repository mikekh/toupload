/**
 * @author MIKE
 */
var Pager = (function() {
    "use strict";
    var PAGE_COUNT = 5;
    
    function Pager() {
         hidePages();
         //$(".page01").show();
    }
    
    var hidePages = function() {
        for (var i = 1; i <= PAGE_COUNT ; i++) {
           $(".page0" + i).hide();
        }        
    };  
    
    Pager.prototype.displayRadioButtonSelection = function() {
        var id = $("input[name=mainGroup]:checked").attr('id');
        hidePages();
        switch(id) {
            case "rdPage01":
                $(".page01").show();                 
            break;
            case "rdPage02": 
                $(".page02").show();
            break;
            case "rdPage03":
                $(".page03").show(); 
            break;
             case "rdPage04":
                $(".page04").show(); 
            break;
             case "rdPage05":
                $(".page05").show(); 
            break;
        }
        //$("#" + id).show();
    };
    return Pager;
})();

$(document).ready(function() {
  "use strict";
  var pager = new Pager();  
  $("input[name=mainGroup]:radio").click(pager.displayRadioButtonSelection);
});